From 56041232238e4e4d3c8d703b27f51b0bc70fd5c8 Mon Sep 17 00:00:00 2001
From: Ard Biesheuvel <ardb@kernel.org>
Date: Mon, 24 Oct 2022 16:50:05 +0200
Subject: [PATCH 03/18] OvmfPkg/VirtNorFlashDxe: remove disk I/O protocol
 implementation

RH-Author: Gerd Hoffmann <None>
RH-MergeRequest: 43: OvmfPkg/VirtNorFlashDxe backport
RH-Jira: RHEL-17587
RH-Acked-by: Laszlo Ersek <lersek@redhat.com>
RH-Commit: [5/20] 0551c3f56f43396cfdc380127565e89d69eb29a3

We only use NOR flash for firmware volumes, either for executable images
or for the variable store. So we have no need for exposing disk I/O on
top of the NOR flash partitions so let's remove it.

Signed-off-by: Ard Biesheuvel <ardb@kernel.org>
Reviewed-by: Sunil V L <sunilvl@ventanamicro.com>
(cherry picked from commit 68d234989b2d6bd8f255577e08bf8be0b1d197bb)
---
 OvmfPkg/VirtNorFlashDxe/VirtNorFlash.c        | 129 ------------------
 OvmfPkg/VirtNorFlashDxe/VirtNorFlash.h        |  29 ----
 .../VirtNorFlashDxe/VirtNorFlashBlockIoDxe.c  | 123 -----------------
 OvmfPkg/VirtNorFlashDxe/VirtNorFlashDxe.c     |   8 --
 4 files changed, 289 deletions(-)
 delete mode 100644 OvmfPkg/VirtNorFlashDxe/VirtNorFlashBlockIoDxe.c

diff --git a/OvmfPkg/VirtNorFlashDxe/VirtNorFlash.c b/OvmfPkg/VirtNorFlashDxe/VirtNorFlash.c
index 59a562efdf..1094d48f7d 100644
--- a/OvmfPkg/VirtNorFlashDxe/VirtNorFlash.c
+++ b/OvmfPkg/VirtNorFlashDxe/VirtNorFlash.c
@@ -788,135 +788,6 @@ NorFlashWriteSingleBlock (
   return EFI_SUCCESS;
 }
 
-/*
-  Although DiskIoDxe will automatically install the DiskIO protocol whenever
-  we install the BlockIO protocol, its implementation is sub-optimal as it reads
-  and writes entire blocks using the BlockIO protocol. In fact we can access
-  NOR flash with a finer granularity than that, so we can improve performance
-  by directly producing the DiskIO protocol.
-*/
-
-/**
-  Read BufferSize bytes from Offset into Buffer.
-
-  @param  This                  Protocol instance pointer.
-  @param  MediaId               Id of the media, changes every time the media is replaced.
-  @param  Offset                The starting byte offset to read from
-  @param  BufferSize            Size of Buffer
-  @param  Buffer                Buffer containing read data
-
-  @retval EFI_SUCCESS           The data was read correctly from the device.
-  @retval EFI_DEVICE_ERROR      The device reported an error while performing the read.
-  @retval EFI_NO_MEDIA          There is no media in the device.
-  @retval EFI_MEDIA_CHANGED     The MediaId does not match the current device.
-  @retval EFI_INVALID_PARAMETER The read request contains device addresses that are not
-                                valid for the device.
-
-**/
-EFI_STATUS
-EFIAPI
-NorFlashDiskIoReadDisk (
-  IN EFI_DISK_IO_PROTOCOL  *This,
-  IN UINT32                MediaId,
-  IN UINT64                DiskOffset,
-  IN UINTN                 BufferSize,
-  OUT VOID                 *Buffer
-  )
-{
-  NOR_FLASH_INSTANCE  *Instance;
-  UINT32              BlockSize;
-  UINT32              BlockOffset;
-  EFI_LBA             Lba;
-
-  Instance = INSTANCE_FROM_DISKIO_THIS (This);
-
-  if (MediaId != Instance->Media.MediaId) {
-    return EFI_MEDIA_CHANGED;
-  }
-
-  BlockSize = Instance->Media.BlockSize;
-  Lba       = (EFI_LBA)DivU64x32Remainder (DiskOffset, BlockSize, &BlockOffset);
-
-  return NorFlashRead (Instance, Lba, BlockOffset, BufferSize, Buffer);
-}
-
-/**
-  Writes a specified number of bytes to a device.
-
-  @param  This       Indicates a pointer to the calling context.
-  @param  MediaId    ID of the medium to be written.
-  @param  Offset     The starting byte offset on the logical block I/O device to write.
-  @param  BufferSize The size in bytes of Buffer. The number of bytes to write to the device.
-  @param  Buffer     A pointer to the buffer containing the data to be written.
-
-  @retval EFI_SUCCESS           The data was written correctly to the device.
-  @retval EFI_WRITE_PROTECTED   The device can not be written to.
-  @retval EFI_DEVICE_ERROR      The device reported an error while performing the write.
-  @retval EFI_NO_MEDIA          There is no media in the device.
-  @retval EFI_MEDIA_CHANGED     The MediaId does not match the current device.
-  @retval EFI_INVALID_PARAMETER The write request contains device addresses that are not
-                                 valid for the device.
-
-**/
-EFI_STATUS
-EFIAPI
-NorFlashDiskIoWriteDisk (
-  IN EFI_DISK_IO_PROTOCOL  *This,
-  IN UINT32                MediaId,
-  IN UINT64                DiskOffset,
-  IN UINTN                 BufferSize,
-  IN VOID                  *Buffer
-  )
-{
-  NOR_FLASH_INSTANCE  *Instance;
-  UINT32              BlockSize;
-  UINT32              BlockOffset;
-  EFI_LBA             Lba;
-  UINTN               RemainingBytes;
-  UINTN               WriteSize;
-  EFI_STATUS          Status;
-
-  Instance = INSTANCE_FROM_DISKIO_THIS (This);
-
-  if (MediaId != Instance->Media.MediaId) {
-    return EFI_MEDIA_CHANGED;
-  }
-
-  BlockSize = Instance->Media.BlockSize;
-  Lba       = (EFI_LBA)DivU64x32Remainder (DiskOffset, BlockSize, &BlockOffset);
-
-  RemainingBytes = BufferSize;
-
-  // Write either all the remaining bytes, or the number of bytes that bring
-  // us up to a block boundary, whichever is less.
-  // (DiskOffset | (BlockSize - 1)) + 1) rounds DiskOffset up to the next
-  // block boundary (even if it is already on one).
-  WriteSize = MIN (RemainingBytes, ((DiskOffset | (BlockSize - 1)) + 1) - DiskOffset);
-
-  do {
-    if (WriteSize == BlockSize) {
-      // Write a full block
-      Status = NorFlashWriteFullBlock (Instance, Lba, Buffer, BlockSize / sizeof (UINT32));
-    } else {
-      // Write a partial block
-      Status = NorFlashWriteSingleBlock (Instance, Lba, BlockOffset, &WriteSize, Buffer);
-    }
-
-    if (EFI_ERROR (Status)) {
-      return Status;
-    }
-
-    // Now continue writing either all the remaining bytes or single blocks.
-    RemainingBytes -= WriteSize;
-    Buffer          = (UINT8 *)Buffer + WriteSize;
-    Lba++;
-    BlockOffset = 0;
-    WriteSize   = MIN (RemainingBytes, BlockSize);
-  } while (RemainingBytes);
-
-  return Status;
-}
-
 EFI_STATUS
 NorFlashReset (
   IN  NOR_FLASH_INSTANCE  *Instance
diff --git a/OvmfPkg/VirtNorFlashDxe/VirtNorFlash.h b/OvmfPkg/VirtNorFlashDxe/VirtNorFlash.h
index e46522a198..7733ee02ee 100644
--- a/OvmfPkg/VirtNorFlashDxe/VirtNorFlash.h
+++ b/OvmfPkg/VirtNorFlashDxe/VirtNorFlash.h
@@ -15,7 +15,6 @@
 #include <Guid/EventGroup.h>
 
 #include <Protocol/BlockIo.h>
-#include <Protocol/DiskIo.h>
 #include <Protocol/FirmwareVolumeBlock.h>
 
 #include <Library/DebugLib.h>
@@ -111,7 +110,6 @@
 #define NOR_FLASH_SIGNATURE  SIGNATURE_32('n', 'o', 'r', '0')
 #define INSTANCE_FROM_FVB_THIS(a)     CR(a, NOR_FLASH_INSTANCE, FvbProtocol, NOR_FLASH_SIGNATURE)
 #define INSTANCE_FROM_BLKIO_THIS(a)   CR(a, NOR_FLASH_INSTANCE, BlockIoProtocol, NOR_FLASH_SIGNATURE)
-#define INSTANCE_FROM_DISKIO_THIS(a)  CR(a, NOR_FLASH_INSTANCE, DiskIoProtocol, NOR_FLASH_SIGNATURE)
 
 typedef struct _NOR_FLASH_INSTANCE NOR_FLASH_INSTANCE;
 
@@ -134,7 +132,6 @@ struct _NOR_FLASH_INSTANCE {
 
   EFI_BLOCK_IO_PROTOCOL                  BlockIoProtocol;
   EFI_BLOCK_IO_MEDIA                     Media;
-  EFI_DISK_IO_PROTOCOL                   DiskIoProtocol;
 
   EFI_FIRMWARE_VOLUME_BLOCK2_PROTOCOL    FvbProtocol;
   VOID                                   *ShadowBuffer;
@@ -203,32 +200,6 @@ NorFlashBlockIoFlushBlocks (
   IN EFI_BLOCK_IO_PROTOCOL  *This
   );
 
-//
-// DiskIO Protocol function EFI_DISK_IO_PROTOCOL.ReadDisk
-//
-EFI_STATUS
-EFIAPI
-NorFlashDiskIoReadDisk (
-  IN EFI_DISK_IO_PROTOCOL  *This,
-  IN UINT32                MediaId,
-  IN UINT64                Offset,
-  IN UINTN                 BufferSize,
-  OUT VOID                 *Buffer
-  );
-
-//
-// DiskIO Protocol function EFI_DISK_IO_PROTOCOL.WriteDisk
-//
-EFI_STATUS
-EFIAPI
-NorFlashDiskIoWriteDisk (
-  IN EFI_DISK_IO_PROTOCOL  *This,
-  IN UINT32                MediaId,
-  IN UINT64                Offset,
-  IN UINTN                 BufferSize,
-  IN VOID                  *Buffer
-  );
-
 //
 // NorFlashFvbDxe.c
 //
diff --git a/OvmfPkg/VirtNorFlashDxe/VirtNorFlashBlockIoDxe.c b/OvmfPkg/VirtNorFlashDxe/VirtNorFlashBlockIoDxe.c
deleted file mode 100644
index ecf152e355..0000000000
--- a/OvmfPkg/VirtNorFlashDxe/VirtNorFlashBlockIoDxe.c
+++ /dev/null
@@ -1,123 +0,0 @@
-/** @file  NorFlashBlockIoDxe.c
-
-  Copyright (c) 2011-2013, ARM Ltd. All rights reserved.<BR>
-
-  SPDX-License-Identifier: BSD-2-Clause-Patent
-
-**/
-
-#include <Library/BaseMemoryLib.h>
-#include <Library/UefiBootServicesTableLib.h>
-
-#include "VirtNorFlash.h"
-
-//
-// BlockIO Protocol function EFI_BLOCK_IO_PROTOCOL.Reset
-//
-EFI_STATUS
-EFIAPI
-NorFlashBlockIoReset (
-  IN EFI_BLOCK_IO_PROTOCOL  *This,
-  IN BOOLEAN                ExtendedVerification
-  )
-{
-  NOR_FLASH_INSTANCE  *Instance;
-
-  Instance = INSTANCE_FROM_BLKIO_THIS (This);
-
-  DEBUG ((DEBUG_BLKIO, "NorFlashBlockIoReset(MediaId=0x%x)\n", This->Media->MediaId));
-
-  return NorFlashReset (Instance);
-}
-
-//
-// BlockIO Protocol function EFI_BLOCK_IO_PROTOCOL.ReadBlocks
-//
-EFI_STATUS
-EFIAPI
-NorFlashBlockIoReadBlocks (
-  IN  EFI_BLOCK_IO_PROTOCOL  *This,
-  IN  UINT32                 MediaId,
-  IN  EFI_LBA                Lba,
-  IN  UINTN                  BufferSizeInBytes,
-  OUT VOID                   *Buffer
-  )
-{
-  NOR_FLASH_INSTANCE  *Instance;
-  EFI_STATUS          Status;
-  EFI_BLOCK_IO_MEDIA  *Media;
-
-  if (This == NULL) {
-    return EFI_INVALID_PARAMETER;
-  }
-
-  Instance = INSTANCE_FROM_BLKIO_THIS (This);
-  Media    = This->Media;
-
-  DEBUG ((DEBUG_BLKIO, "NorFlashBlockIoReadBlocks(MediaId=0x%x, Lba=%ld, BufferSize=0x%x bytes (%d kB), BufferPtr @ 0x%08x)\n", MediaId, Lba, BufferSizeInBytes, BufferSizeInBytes, Buffer));
-
-  if (!Media) {
-    Status = EFI_INVALID_PARAMETER;
-  } else if (!Media->MediaPresent) {
-    Status = EFI_NO_MEDIA;
-  } else if (Media->MediaId != MediaId) {
-    Status = EFI_MEDIA_CHANGED;
-  } else if ((Media->IoAlign > 2) && (((UINTN)Buffer & (Media->IoAlign - 1)) != 0)) {
-    Status = EFI_INVALID_PARAMETER;
-  } else {
-    Status = NorFlashReadBlocks (Instance, Lba, BufferSizeInBytes, Buffer);
-  }
-
-  return Status;
-}
-
-//
-// BlockIO Protocol function EFI_BLOCK_IO_PROTOCOL.WriteBlocks
-//
-EFI_STATUS
-EFIAPI
-NorFlashBlockIoWriteBlocks (
-  IN  EFI_BLOCK_IO_PROTOCOL  *This,
-  IN  UINT32                 MediaId,
-  IN  EFI_LBA                Lba,
-  IN  UINTN                  BufferSizeInBytes,
-  IN  VOID                   *Buffer
-  )
-{
-  NOR_FLASH_INSTANCE  *Instance;
-  EFI_STATUS          Status;
-
-  Instance = INSTANCE_FROM_BLKIO_THIS (This);
-
-  DEBUG ((DEBUG_BLKIO, "NorFlashBlockIoWriteBlocks(MediaId=0x%x, Lba=%ld, BufferSize=0x%x bytes, BufferPtr @ 0x%08x)\n", MediaId, Lba, BufferSizeInBytes, Buffer));
-
-  if ( !This->Media->MediaPresent ) {
-    Status = EFI_NO_MEDIA;
-  } else if ( This->Media->MediaId != MediaId ) {
-    Status = EFI_MEDIA_CHANGED;
-  } else if ( This->Media->ReadOnly ) {
-    Status = EFI_WRITE_PROTECTED;
-  } else {
-    Status = NorFlashWriteBlocks (Instance, Lba, BufferSizeInBytes, Buffer);
-  }
-
-  return Status;
-}
-
-//
-// BlockIO Protocol function EFI_BLOCK_IO_PROTOCOL.FlushBlocks
-//
-EFI_STATUS
-EFIAPI
-NorFlashBlockIoFlushBlocks (
-  IN EFI_BLOCK_IO_PROTOCOL  *This
-  )
-{
-  // No Flush required for the NOR Flash driver
-  // because cache operations are not permitted.
-
-  DEBUG ((DEBUG_BLKIO, "NorFlashBlockIoFlushBlocks: Function NOT IMPLEMENTED (not required).\n"));
-
-  // Nothing to do so just return without error
-  return EFI_SUCCESS;
-}
diff --git a/OvmfPkg/VirtNorFlashDxe/VirtNorFlashDxe.c b/OvmfPkg/VirtNorFlashDxe/VirtNorFlashDxe.c
index 819425545e..4875b057d5 100644
--- a/OvmfPkg/VirtNorFlashDxe/VirtNorFlashDxe.c
+++ b/OvmfPkg/VirtNorFlashDxe/VirtNorFlashDxe.c
@@ -58,12 +58,6 @@ NOR_FLASH_INSTANCE  mNorFlashInstanceTemplate = {
     1,     // LogicalBlocksPerPhysicalBlock
   }, // Media;
 
-  {
-    EFI_DISK_IO_PROTOCOL_REVISION, // Revision
-    NorFlashDiskIoReadDisk,        // ReadDisk
-    NorFlashDiskIoWriteDisk        // WriteDisk
-  },
-
   {
     FvbGetAttributes,      // GetAttributes
     FvbSetAttributes,      // SetAttributes
@@ -159,8 +153,6 @@ NorFlashCreateInstance (
                     &Instance->DevicePath,
                     &gEfiBlockIoProtocolGuid,
                     &Instance->BlockIoProtocol,
-                    &gEfiDiskIoProtocolGuid,
-                    &Instance->DiskIoProtocol,
                     NULL
                     );
     if (EFI_ERROR (Status)) {
-- 
2.41.0

