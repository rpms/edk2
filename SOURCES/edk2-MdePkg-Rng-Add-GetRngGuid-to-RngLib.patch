From b466e2545e25ebb2004ae9b9f95c6c2f60d1f168 Mon Sep 17 00:00:00 2001
From: Jon Maloy <jmaloy@redhat.com>
Date: Thu, 20 Jun 2024 16:08:28 -0400
Subject: [PATCH 21/31] MdePkg/Rng: Add GetRngGuid() to RngLib

RH-Author: Jon Maloy <jmaloy@redhat.com>
RH-MergeRequest: 77: UINT32 overflow in S3 ResumeCount and Pixiefail fixes
RH-Jira: RHEL-21854 RHEL-21856 RHEL-40099
RH-Acked-by: Gerd Hoffmann <None>
RH-Commit: [21/31] 54783ad88ba101c620240aa463c5d758fa416c31

JIRA: https://issues.redhat.com/browse/RHEL-21856
Upstream: Merged
CVE: CVE-2023-45237

commit 5443c2dc310d2c8eb15fb8eefd5057342e78cd0d
Author: Pierre Gondois <pierre.gondois@arm.com>
Date:   Fri Aug 11 16:33:08 2023 +0200

    MdePkg/Rng: Add GetRngGuid() to RngLib

    The EFI_RNG_PROTOCOL can use the RngLib. The RngLib has multiple
    implementations, some of them are unsafe (e.g. BaseRngLibTimerLib).
    To allow the RngDxe to detect when such implementation is used,
    add a GetRngGuid() function to the RngLib.

    Signed-off-by: Pierre Gondois <pierre.gondois@arm.com>
    Reviewed-by: Liming Gao <gaoliming@byosoft.com.cn>
    Reviewed-by: Sami Mujawar <sami.mujawar@arm.com>
    Acked-by: Ard Biesheuvel <ardb@kernel.org>
    Tested-by: Kun Qin <kun.qin@microsoft.com>

Signed-off-by: Jon Maloy <jmaloy@redhat.com>
---
 .../BaseRngLibTimerLib/BaseRngLibTimerLib.inf |  4 ++
 .../Library/BaseRngLibTimerLib/RngLibTimer.c  | 28 +++++++++++++
 MdePkg/Include/Library/RngLib.h               | 19 ++++++++-
 MdePkg/Library/BaseRngLib/AArch64/Rndr.c      | 42 +++++++++++++++++++
 MdePkg/Library/BaseRngLib/BaseRngLib.inf      | 10 +++++
 MdePkg/Library/BaseRngLib/Rand/RdRand.c       | 26 ++++++++++++
 .../Library/BaseRngLibNull/BaseRngLibNull.c   | 22 ++++++++++
 .../Library/BaseRngLibTimerLib/RngLibTimer.c  | 23 ++++++++++
 MdePkg/Library/DxeRngLib/DxeRngLib.c          | 28 +++++++++++++
 9 files changed, 201 insertions(+), 1 deletion(-)

diff --git a/MdeModulePkg/Library/BaseRngLibTimerLib/BaseRngLibTimerLib.inf b/MdeModulePkg/Library/BaseRngLibTimerLib/BaseRngLibTimerLib.inf
index f729001060..8461260cc8 100644
--- a/MdeModulePkg/Library/BaseRngLibTimerLib/BaseRngLibTimerLib.inf
+++ b/MdeModulePkg/Library/BaseRngLibTimerLib/BaseRngLibTimerLib.inf
@@ -29,6 +29,10 @@
 
 [Packages]
   MdePkg/MdePkg.dec
+  MdeModulePkg/MdeModulePkg.dec
+
+[Guids]
+  gEdkiiRngAlgorithmUnSafe
 
 [LibraryClasses]
   BaseLib
diff --git a/MdeModulePkg/Library/BaseRngLibTimerLib/RngLibTimer.c b/MdeModulePkg/Library/BaseRngLibTimerLib/RngLibTimer.c
index 980854d67b..28ff46c71f 100644
--- a/MdeModulePkg/Library/BaseRngLibTimerLib/RngLibTimer.c
+++ b/MdeModulePkg/Library/BaseRngLibTimerLib/RngLibTimer.c
@@ -2,14 +2,18 @@
   BaseRng Library that uses the TimerLib to provide reasonably random numbers.
   Do not use this on a production system.
 
+  Copyright (c) 2023, Arm Limited. All rights reserved.
   Copyright (c) Microsoft Corporation.
   SPDX-License-Identifier: BSD-2-Clause-Patent
 **/
 
 #include <Base.h>
+#include <Uefi.h>
 #include <Library/BaseLib.h>
+#include <Library/BaseMemoryLib.h>
 #include <Library/DebugLib.h>
 #include <Library/TimerLib.h>
+#include <Guid/RngAlgorithm.h>
 
 #define DEFAULT_DELAY_TIME_IN_MICROSECONDS  10
 
@@ -190,3 +194,27 @@ GetRandomNumber128 (
   // Read second 64 bits
   return GetRandomNumber64 (++Rand);
 }
+
+/**
+  Get a GUID identifying the RNG algorithm implementation.
+
+  @param [out] RngGuid  If success, contains the GUID identifying
+                        the RNG algorithm implementation.
+
+  @retval EFI_SUCCESS             Success.
+  @retval EFI_UNSUPPORTED         Not supported.
+  @retval EFI_INVALID_PARAMETER   Invalid parameter.
+**/
+EFI_STATUS
+EFIAPI
+GetRngGuid (
+  GUID  *RngGuid
+  )
+{
+  if (RngGuid == NULL) {
+    return EFI_INVALID_PARAMETER;
+  }
+
+  CopyMem (RngGuid, &gEdkiiRngAlgorithmUnSafe, sizeof (*RngGuid));
+  return EFI_SUCCESS;
+}
diff --git a/MdePkg/Include/Library/RngLib.h b/MdePkg/Include/Library/RngLib.h
index 05e513022e..801aa6d5bd 100644
--- a/MdePkg/Include/Library/RngLib.h
+++ b/MdePkg/Include/Library/RngLib.h
@@ -1,6 +1,7 @@
 /** @file
   Provides random number generator services.
 
+Copyright (c) 2023, Arm Limited. All rights reserved.<BR>
 Copyright (c) 2015, Intel Corporation. All rights reserved.<BR>
 SPDX-License-Identifier: BSD-2-Clause-Patent
 
@@ -77,4 +78,20 @@ GetRandomNumber128 (
   OUT     UINT64                    *Rand
   );
 
-#endif  // __RNG_LIB_H__
+/**
+  Get a GUID identifying the RNG algorithm implementation.
+
+  @param [out] RngGuid  If success, contains the GUID identifying
+                        the RNG algorithm implementation.
+
+  @retval EFI_SUCCESS             Success.
+  @retval EFI_UNSUPPORTED         Not supported.
+  @retval EFI_INVALID_PARAMETER   Invalid parameter.
+**/
+EFI_STATUS
+EFIAPI
+GetRngGuid (
+  GUID  *RngGuid
+  );
+
+#endif // __RNG_LIB_H__
diff --git a/MdePkg/Library/BaseRngLib/AArch64/Rndr.c b/MdePkg/Library/BaseRngLib/AArch64/Rndr.c
index c9f8c813ed..7641314a54 100644
--- a/MdePkg/Library/BaseRngLib/AArch64/Rndr.c
+++ b/MdePkg/Library/BaseRngLib/AArch64/Rndr.c
@@ -2,6 +2,7 @@
   Random number generator service that uses the RNDR instruction
   to provide pseudorandom numbers.
 
+  Copyright (c) 2023, Arm Limited. All rights reserved.<BR>
   Copyright (c) 2021, NUVIA Inc. All rights reserved.<BR>
   Copyright (c) 2015, Intel Corporation. All rights reserved.<BR>
 
@@ -11,6 +12,7 @@
 
 #include <Uefi.h>
 #include <Library/BaseLib.h>
+#include <Library/BaseMemoryLib.h>
 #include <Library/DebugLib.h>
 #include <Library/RngLib.h>
 
@@ -137,3 +139,43 @@ ArchIsRngSupported (
 {
   return mRndrSupported;
 }
+
+/**
+  Get a GUID identifying the RNG algorithm implementation.
+
+  @param [out] RngGuid  If success, contains the GUID identifying
+                        the RNG algorithm implementation.
+
+  @retval EFI_SUCCESS             Success.
+  @retval EFI_UNSUPPORTED         Not supported.
+  @retval EFI_INVALID_PARAMETER   Invalid parameter.
+**/
+EFI_STATUS
+EFIAPI
+GetRngGuid (
+  GUID  *RngGuid
+  )
+{
+  GUID  *RngLibGuid;
+
+  if (RngGuid == NULL) {
+    return EFI_INVALID_PARAMETER;
+  }
+
+  if (!mRndrSupported) {
+    return EFI_UNSUPPORTED;
+  }
+
+  //
+  // If the platform advertises the algorithm behind RNDR instruction,
+  // use it. Otherwise use gEfiRngAlgorithmArmRndr.
+  //
+  RngLibGuid = PcdGetPtr (PcdCpuRngSupportedAlgorithm);
+  if (!IsZeroGuid (RngLibGuid)) {
+    CopyMem (RngGuid, RngLibGuid, sizeof (*RngGuid));
+  } else {
+    CopyMem (RngGuid, &gEfiRngAlgorithmArmRndr, sizeof (*RngGuid));
+  }
+
+  return EFI_SUCCESS;
+}
diff --git a/MdePkg/Library/BaseRngLib/BaseRngLib.inf b/MdePkg/Library/BaseRngLib/BaseRngLib.inf
index 1fcceb9414..49503b139b 100644
--- a/MdePkg/Library/BaseRngLib/BaseRngLib.inf
+++ b/MdePkg/Library/BaseRngLib/BaseRngLib.inf
@@ -4,6 +4,7 @@
 #  BaseRng Library that uses CPU RNG instructions (e.g. RdRand) to
 #  provide random numbers.
 #
+#  Copyright (c) 2023, Arm Limited. All rights reserved.<BR>
 #  Copyright (c) 2021, NUVIA Inc. All rights reserved.<BR>
 #  Copyright (c) 2015, Intel Corporation. All rights reserved.<BR>
 #
@@ -43,9 +44,18 @@
   AArch64/ArmReadIdIsar0.asm | MSFT
   AArch64/ArmRng.asm         | MSFT
 
+[Guids.AARCH64]
+  gEfiRngAlgorithmArmRndr
+
+[Guids.Ia32, Guids.X64]
+  gEfiRngAlgorithmSp80090Ctr256Guid
+
 [Packages]
   MdePkg/MdePkg.dec
 
+[Pcd.AARCH64]
+  gEfiMdePkgTokenSpaceGuid.PcdCpuRngSupportedAlgorithm
+
 [LibraryClasses]
   BaseLib
   DebugLib
diff --git a/MdePkg/Library/BaseRngLib/Rand/RdRand.c b/MdePkg/Library/BaseRngLib/Rand/RdRand.c
index 09fb875ac3..aee8ea04e8 100644
--- a/MdePkg/Library/BaseRngLib/Rand/RdRand.c
+++ b/MdePkg/Library/BaseRngLib/Rand/RdRand.c
@@ -2,6 +2,7 @@
   Random number generator services that uses RdRand instruction access
   to provide high-quality random numbers.
 
+Copyright (c) 2023, Arm Limited. All rights reserved.<BR>
 Copyright (c) 2021, NUVIA Inc. All rights reserved.<BR>
 Copyright (c) 2015, Intel Corporation. All rights reserved.<BR>
 
@@ -11,6 +12,7 @@ SPDX-License-Identifier: BSD-2-Clause-Patent
 
 #include <Uefi.h>
 #include <Library/BaseLib.h>
+#include <Library/BaseMemoryLib.h>
 #include <Library/DebugLib.h>
 
 #include "BaseRngLibInternals.h"
@@ -129,3 +131,27 @@ ArchIsRngSupported (
   */
   return TRUE;
 }
+
+/**
+  Get a GUID identifying the RNG algorithm implementation.
+
+  @param [out] RngGuid  If success, contains the GUID identifying
+                        the RNG algorithm implementation.
+
+  @retval EFI_SUCCESS             Success.
+  @retval EFI_UNSUPPORTED         Not supported.
+  @retval EFI_INVALID_PARAMETER   Invalid parameter.
+**/
+EFI_STATUS
+EFIAPI
+GetRngGuid (
+  GUID  *RngGuid
+  )
+{
+  if (RngGuid == NULL) {
+    return EFI_INVALID_PARAMETER;
+  }
+
+  CopyMem (RngGuid, &gEfiRngAlgorithmSp80090Ctr256Guid, sizeof (*RngGuid));
+  return EFI_SUCCESS;
+}
diff --git a/MdePkg/Library/BaseRngLibNull/BaseRngLibNull.c b/MdePkg/Library/BaseRngLibNull/BaseRngLibNull.c
index cad30599ea..34a18e6a4d 100644
--- a/MdePkg/Library/BaseRngLibNull/BaseRngLibNull.c
+++ b/MdePkg/Library/BaseRngLibNull/BaseRngLibNull.c
@@ -1,13 +1,16 @@
 /** @file
   Null version of Random number generator services.
 
+Copyright (c) 2023, Arm Limited. All rights reserved.<BR>
 Copyright (c) 2019, Intel Corporation. All rights reserved.<BR>
 SPDX-License-Identifier: BSD-2-Clause-Patent
 
 **/
 
+#include <Uefi.h>
 #include <Library/DebugLib.h>
 #include <Library/RngLib.h>
+#include <Protocol/Rng.h>
 
 /**
   Generates a 16-bit random number.
@@ -92,3 +95,22 @@ GetRandomNumber128 (
   ASSERT (FALSE);
   return FALSE;
 }
+
+/**
+  Get a GUID identifying the RNG algorithm implementation.
+
+  @param [out] RngGuid  If success, contains the GUID identifying
+                        the RNG algorithm implementation.
+
+  @retval EFI_SUCCESS             Success.
+  @retval EFI_UNSUPPORTED         Not supported.
+  @retval EFI_INVALID_PARAMETER   Invalid parameter.
+**/
+EFI_STATUS
+EFIAPI
+GetRngGuid (
+  GUID  *RngGuid
+  )
+{
+  return EFI_UNSUPPORTED;
+}
diff --git a/MdePkg/Library/BaseRngLibTimerLib/RngLibTimer.c b/MdePkg/Library/BaseRngLibTimerLib/RngLibTimer.c
index 6b8392162b..7337500fec 100644
--- a/MdePkg/Library/BaseRngLibTimerLib/RngLibTimer.c
+++ b/MdePkg/Library/BaseRngLibTimerLib/RngLibTimer.c
@@ -209,3 +209,26 @@ GetRandomNumber128 (
   // Read second 64 bits
   return GetRandomNumber64 (++Rand);
 }
+
+/**
+  Get a GUID identifying the RNG algorithm implementation.
+
+  @param [out] RngGuid  If success, contains the GUID identifying
+                        the RNG algorithm implementation.
+
+  @retval EFI_SUCCESS             Success.
+  @retval EFI_UNSUPPORTED         Not supported.
+  @retval EFI_INVALID_PARAMETER   Invalid parameter.
+**/
+RETURN_STATUS
+EFIAPI
+GetRngGuid (
+  GUID  *RngGuid
+  )
+{
+  /* This implementation is to be replaced by its MdeModulePkg copy.
+   * The cause being that some GUIDs (gEdkiiRngAlgorithmUnSafe) cannot
+   * be defined in the MdePkg.
+   */
+  return RETURN_UNSUPPORTED;
+}
diff --git a/MdePkg/Library/DxeRngLib/DxeRngLib.c b/MdePkg/Library/DxeRngLib/DxeRngLib.c
index 4b2fc1cde5..20248b4107 100644
--- a/MdePkg/Library/DxeRngLib/DxeRngLib.c
+++ b/MdePkg/Library/DxeRngLib/DxeRngLib.c
@@ -1,6 +1,7 @@
 /** @file
  Provides an implementation of the library class RngLib that uses the Rng protocol.
 
+ Copyright (c) 2023, Arm Limited. All rights reserved.
  Copyright (c) Microsoft Corporation. All rights reserved.
  SPDX-License-Identifier: BSD-2-Clause-Patent
 
@@ -204,3 +205,30 @@ GetRandomNumber128 (
   }
   return TRUE;
 }
+
+/**
+  Get a GUID identifying the RNG algorithm implementation.
+
+  @param [out] RngGuid  If success, contains the GUID identifying
+                        the RNG algorithm implementation.
+
+  @retval EFI_SUCCESS             Success.
+  @retval EFI_UNSUPPORTED         Not supported.
+  @retval EFI_INVALID_PARAMETER   Invalid parameter.
+**/
+EFI_STATUS
+EFIAPI
+GetRngGuid (
+  GUID  *RngGuid
+  )
+{
+  /* It is not possible to know beforehand which Rng algorithm will
+   * be used by this library.
+   * This API is mainly used by RngDxe. RngDxe relies on the RngLib.
+   * The RngLib|DxeRngLib.inf implementation locates and uses an installed
+   * EFI_RNG_PROTOCOL.
+   * It is thus not possible to have both RngDxe and RngLib|DxeRngLib.inf.
+   * and it is ok not to support this API.
+   */
+  return EFI_UNSUPPORTED;
+}
-- 
2.39.3

