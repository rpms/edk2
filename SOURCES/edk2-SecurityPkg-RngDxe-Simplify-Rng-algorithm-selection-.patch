From 1a0bf45b088e05f6eb7edaa0d24aec894ea3491b Mon Sep 17 00:00:00 2001
From: Jon Maloy <jmaloy@redhat.com>
Date: Thu, 20 Jun 2024 16:11:16 -0400
Subject: [PATCH 23/31] SecurityPkg/RngDxe: Simplify Rng algorithm selection
 for Arm

RH-Author: Jon Maloy <jmaloy@redhat.com>
RH-MergeRequest: 77: UINT32 overflow in S3 ResumeCount and Pixiefail fixes
RH-Jira: RHEL-21854 RHEL-21856 RHEL-40099
RH-Acked-by: Gerd Hoffmann <None>
RH-Commit: [23/31] 21b2854eed63bf5d406cfec5ac03b9ae3901a679

JIRA: https://issues.redhat.com/browse/RHEL-21856
Upstream: Merged
CVE: CVE-2023-45237

commit ff7ddc02b273f9159ef46fdb67d99062f8e598d9
Author: Pierre Gondois <pierre.gondois@arm.com>
Date:   Fri Aug 11 16:33:10 2023 +0200

    SecurityPkg/RngDxe: Simplify Rng algorithm selection for Arm

    The first element of mAvailableAlgoArray is defined as the default
    Rng algorithm to use. Don't go through the array at each RngGetRNG()
    call and just return the first element of the array.

    Signed-off-by: Pierre Gondois <pierre.gondois@arm.com>
    Reviewed-by: Sami Mujawar <sami.mujawar@arm.com>
    Acked-by: Ard Biesheuvel <ardb@kernel.org>
    Acked-by: Jiewen Yao <Jiewen.yao@intel.com>
    Tested-by: Kun Qin <kun.qin@microsoft.com>

Signed-off-by: Jon Maloy <jmaloy@redhat.com>
---
 .../RandomNumberGenerator/RngDxe/ArmRngDxe.c    | 17 ++++-------------
 1 file changed, 4 insertions(+), 13 deletions(-)

diff --git a/SecurityPkg/RandomNumberGenerator/RngDxe/ArmRngDxe.c b/SecurityPkg/RandomNumberGenerator/RngDxe/ArmRngDxe.c
index 2fc36fc186..7249904413 100644
--- a/SecurityPkg/RandomNumberGenerator/RngDxe/ArmRngDxe.c
+++ b/SecurityPkg/RandomNumberGenerator/RngDxe/ArmRngDxe.c
@@ -77,7 +77,6 @@ RngGetRNG (
   )
 {
   EFI_STATUS  Status;
-  UINTN       Index;
   GUID        RngGuid;
 
   if ((This == NULL) || (RNGValueLength == 0) || (RNGValue == NULL)) {
@@ -88,21 +87,13 @@ RngGetRNG (
     //
     // Use the default RNG algorithm if RNGAlgorithm is NULL.
     //
-    for (Index = 0; Index < mAvailableAlgoArrayCount; Index++) {
-      if (!IsZeroGuid (&mAvailableAlgoArray[Index])) {
-        RNGAlgorithm = &mAvailableAlgoArray[Index];
-        goto FoundAlgo;
-      }
-    }
-
-    if (Index == mAvailableAlgoArrayCount) {
-      // No algorithm available.
-      ASSERT (Index != mAvailableAlgoArrayCount);
-      return EFI_DEVICE_ERROR;
+    if (mAvailableAlgoArrayCount != 0) {
+      RNGAlgorithm = &mAvailableAlgoArray[0];
+    } else {
+      return EFI_UNSUPPORTED;
     }
   }
 
-FoundAlgo:
   Status = GetRngGuid (&RngGuid);
   if (!EFI_ERROR (Status) &&
       CompareGuid (RNGAlgorithm, &RngGuid))
-- 
2.39.3

